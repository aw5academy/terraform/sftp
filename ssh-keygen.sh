#!/bin/bash

if [ ! -e example.pem ]; then
  ssh-keygen -q -t rsa -N '' -C "example transfer user" -f example.pem
fi

PUBLIC_KEY="`cat example.pem.pub`"
jq -n --arg public_key "$PUBLIC_KEY" '{"public_key":$public_key}'
