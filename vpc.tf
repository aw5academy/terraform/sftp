resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = "true"
  tags = {
    Name = var.stack-name
  }
}

resource "aws_internet_gateway" "main" {
  tags = {
    Name = var.stack-name
  }
  vpc_id = aws_vpc.main.id
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/transfer_server seems to use the default
# security group and does not allow a custom group to be used so we update the default group to allow all inbound SSH
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.main.id
  ingress {
    protocol    = "TCP"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

################################################
# Subnets
################################################
resource "aws_subnet" "firewall-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.stack-name}-firewall-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "firewall-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.stack-name}-firewall-b"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "sftp-a" {
  availability_zone       = "us-east-1a"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.stack-name}-sftp-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "sftp-b" {
  availability_zone       = "us-east-1b"
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.stack-name}-sftp-b"
  }
  vpc_id = aws_vpc.main.id
}

################################################
# Route Tables
################################################
resource "aws_route_table" "igw" {
  tags = {
    Name = "${var.stack-name}-igw"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "igw-a" {
  destination_cidr_block = aws_subnet.sftp-a.cidr_block
  vpc_endpoint_id        = local.firewall-endpoint-a
  route_table_id         = aws_route_table.igw.id
}

resource "aws_route" "igw-b" {
  destination_cidr_block = aws_subnet.sftp-b.cidr_block
  vpc_endpoint_id        = local.firewall-endpoint-b
  route_table_id         = aws_route_table.igw.id
}

resource "aws_route_table_association" "igw" {
  gateway_id     = aws_internet_gateway.main.id
  route_table_id = aws_route_table.igw.id
}

resource "aws_route_table" "firewall" {
  tags = {
    Name = "${var.stack-name}-firewall"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "firewall" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
  route_table_id         = aws_route_table.firewall.id
}

resource "aws_route_table_association" "firewall-a" {
  route_table_id = aws_route_table.firewall.id
  subnet_id      = aws_subnet.firewall-a.id
}

resource "aws_route_table_association" "firewall-b" {
  route_table_id = aws_route_table.firewall.id
  subnet_id      = aws_subnet.firewall-b.id
}

resource "aws_route_table" "sftp-a" {
  tags = {
    Name = "${var.stack-name}-sftp-a"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "sftp-a" {
  destination_cidr_block = "0.0.0.0/0"
  vpc_endpoint_id        = local.firewall-endpoint-a
  route_table_id         = aws_route_table.sftp-a.id
}

resource "aws_route_table_association" "sftp-a" {
  route_table_id = aws_route_table.sftp-a.id
  subnet_id      = aws_subnet.sftp-a.id
}

resource "aws_route_table" "sftp-b" {
  tags = {
    Name = "${var.stack-name}-sftp-b"
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_route" "sftp-b" {
  destination_cidr_block = "0.0.0.0/0"
  vpc_endpoint_id        = local.firewall-endpoint-b
  route_table_id         = aws_route_table.sftp-b.id
}

resource "aws_route_table_association" "sftp-b" {
  route_table_id = aws_route_table.sftp-b.id
  subnet_id      = aws_subnet.sftp-b.id
}

################################################
# Elastic IPs
################################################
resource "aws_eip" "sftp-a" {
  tags = {
    Name = "${var.stack-name}-sftp-a"
  }
  vpc = true
}

resource "aws_eip" "sftp-b" {
  tags = {
    Name = "${var.stack-name}-sftp-b"
  }
  vpc = true
}
