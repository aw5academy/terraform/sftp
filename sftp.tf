data "aws_iam_policy_document" "transfer" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "transfer.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "transfer" {
  assume_role_policy = data.aws_iam_policy_document.transfer.json
  name               = "${var.stack-name}-transfer"
  tags = {
    Name = "${var.stack-name}-transfer"
  }
}

resource "aws_iam_role_policy" "transfer" {
  name   = "transfer"
  policy = templatefile("${path.module}/templates/transfer-policy.json.tpl", {})
  role   = aws_iam_role.transfer.id
}

resource "aws_transfer_server" "main" {
  identity_provider_type = "SERVICE_MANAGED"
  endpoint_type          = "VPC"
  endpoint_details {
    address_allocation_ids = [
      aws_eip.sftp-a.id, aws_eip.sftp-b.id
    ]
    subnet_ids             = [
      aws_subnet.sftp-a.id, aws_subnet.sftp-b.id
    ]
    vpc_id                 = aws_vpc.main.id
  }
  logging_role           = aws_iam_role.transfer.arn
  tags = {
    Name = "${var.stack-name}-transfer"
  }
}

resource "aws_iam_role" "transfer-user" {
  assume_role_policy = data.aws_iam_policy_document.transfer.json
  name               = "${var.stack-name}-transfer-user"
  tags = {
    Name = "${var.stack-name}-transfer-user"
  }
}

resource "aws_iam_role_policy" "transfer-user" {
  name   = "transfer-user"
  policy = templatefile("${path.module}/templates/transfer-user-policy.json.tpl", 
    {
      s3_bucket_arn = aws_s3_bucket.files.arn
    }
  )
  role   = aws_iam_role.transfer-user.id
}

resource "aws_transfer_user" "example" {
  home_directory = "/${aws_s3_bucket.files.id}"
  server_id      = aws_transfer_server.main.id
  user_name      = "example"
  role           = aws_iam_role.transfer-user.arn
}

data "external" "ssh-keygen" {
  program = ["bash", "${path.module}/ssh-keygen.sh"]
}

resource "aws_transfer_ssh_key" "example" {
  server_id = aws_transfer_server.main.id
  user_name = aws_transfer_user.example.user_name
  body      = data.external.ssh-keygen.result["public_key"]
}
