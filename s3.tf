resource "random_string" "random" {
  length  = 12
  special = false
  upper   = false
  number  = false
}

resource "aws_s3_bucket" "files" {
  acl           = "private"
  bucket        = "${var.stack-name}-${random_string.random.result}"
  force_destroy = true
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name = "${var.stack-name}-${random_string.random.result}"
  }
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_public_access_block" "files" {
  block_public_acls       = true
  block_public_policy     = true
  bucket                  = aws_s3_bucket.files.id
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_object" "sample" {
  bucket = aws_s3_bucket.files.id
  key    = "sample-file.txt"
  source = "sample-file.txt"
  etag   = filemd5("sample-file.txt")
}
