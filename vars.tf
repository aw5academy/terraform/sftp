variable "stack-name" {
  description = "The stack identifier."
  default     = "file-share"
}
