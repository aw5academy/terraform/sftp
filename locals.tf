locals {
  firewall-endpoint-b = [ for attachment in flatten(aws_networkfirewall_firewall.main.firewall_status[0].sync_states)[0].attachment : attachment.endpoint_id ][0]
  firewall-endpoint-a = [ for attachment in flatten(aws_networkfirewall_firewall.main.firewall_status[0].sync_states)[1].attachment : attachment.endpoint_id ][0]
}
