#!/bin/bash

PUBLIC_IP="`curl ifconfig.me`/32"
jq -n --arg public_ip "$PUBLIC_IP" '{"public_ip":$public_ip}'
