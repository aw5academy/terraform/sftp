resource "aws_networkfirewall_firewall" "main" {
  name                = var.stack-name
  firewall_policy_arn = aws_networkfirewall_firewall_policy.main.arn
  vpc_id              = aws_vpc.main.id
  subnet_mapping {
    subnet_id = aws_subnet.firewall-a.id
  }
  subnet_mapping {
    subnet_id = aws_subnet.firewall-b.id
  }
  tags = {
    Name = var.stack-name
  }
}

resource "aws_networkfirewall_firewall_policy" "main" {
  name = var.stack-name
  firewall_policy {
    stateless_default_actions          = ["aws:forward_to_sfe"]
    stateless_fragment_default_actions = ["aws:pass"]
    stateful_rule_group_reference {
      resource_arn = aws_networkfirewall_rule_group.main.arn
    }
  }
  tags = {
    Name = var.stack-name
  }
}

data "external" "ifconfig" {
  program = ["bash", "${path.module}/ifconfig.sh"]
}

resource "aws_networkfirewall_rule_group" "main" {
  capacity = 100
  name     = var.stack-name
  type     = "STATEFUL"
  rule_group {
    rules_source {
      stateful_rule {
        action = "PASS"
        header {
          destination      = "0.0.0.0/0"
          destination_port = "Any"
          direction        = "ANY"
          protocol         = "SSH"
          source           = data.external.ifconfig.result["public_ip"]
          source_port      = "Any"
        }
        rule_option {
          keyword  = "sid:1"
          settings = []
        }
      }
    }
  }
  tags = {
    Name = var.stack-name
  }
}
