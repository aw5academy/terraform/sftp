output "bucket-name" {
  value = aws_s3_bucket.files.id
}

output "sftp-endpoint" {
  value = aws_transfer_server.main.endpoint
}
